const mongoose = require("mongoose");

const { NOTES_APP_MONGODB_HOST, NOTES_APP_MONGODB_DATABASE } = process.env;
//si estas en tu equipo local
//const MONGODB_URI = `mongodb://localhost/mean-crud`;
//si estas en AWS con docker
//const MONGODB_URI = `mongodb://mongo:27017/mean-crud`;
const MONGODB_URI = `mongodb://mongo:27017/mean-crud`;

mongoose
  .connect(MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true 
  })
  .then(db => console.log("DB is connected"))
  .catch(err => console.error(err));
